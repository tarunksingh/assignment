var asyncLoop = require('node-async-loop');
var elasticsearch = require('elasticsearch');
var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "Asd11zxc",
    // password: "Asd11zxc", Asd11zxc
    database: "ES"
});

con.connect(function (err) {
    if (err) console.log(err.message);
    console.log("Connected to databse ES!");
});

var client = new elasticsearch.Client({
    host: "http://elastic:Getup@localhost:9200",
    requestTimeout: 150000
});

// start time is the varible that stores the time of begining of the program
start_time = new Date()


// TODO change the argument nanme : DONE 
// This function searches from ES create the string and then update into mysql just provide the one element(firstName) of array 
function search_es(firstname, next) {

    var query = {
        "query": {
            "bool": {
                "must": [{
                    "multi_match": {
                        "fields": [
                            "firstname"
                        ],
                        "fuzziness": "AUTO:4,7",
                        "query": firstname
                    }
                }],
                "must_not": [{
                    "multi_match": {
                        "fields": [
                            "firstname_phonetic"
                        ],
                        "query": firstname
                    }
                }]
            }
        }
    }


    client.search({
        index: 'firstname_all_2',
        type: 'document',
        body: query
    }, function (error, response, status) {
        if (error) console.log('Error from elastic search:' + error);
        else {
            str = ""
            response.hits.hits.forEach(function (hit) {

                if (firstname[0] == 'b' || firstname[0] == 'v') {

                    first_char = hit._source.firstname[0]
                    if (first_char != 'b' && first_char != 'v') {
                        // str = str + hit._source.firstname + ","
                        str = str.concat(hit._source.firstname, ",")

                    }

                } else {
                    // str = str + hit._source.firstname + ","
                    str = str.concat(hit._source.firstname, ",")

                }
            })

            str = remove_last(str)
            var sql = `UPDATE PAN_INDIA_IGNORE SET ignore_firstName = "${str}", firstName_Phonetic="${firstname}" WHERE firstName="${firstname}"`;
            con.query(sql, function (err, result) {
                if (err) return console.log('data did not inserted ' + err.message);
                if (!err) console.log((new Date() - start_time) / 1000)
            });

            next();
        }
    });
}


// This counts the no of row present in the table and return that no to the next callback 
function Mainfunction(q, callback) {
    con.query(q, (err, result) => {
        if (err) {
            callback('Failed to count the total no of rows', undefined)
        } else {
            console.log("The total count is : " + result[0].total_rows);
            callback(undefined, result[0].total_rows)
        }
    })
}

// TODO : simplify this : DONE 
// this function returns an array of ranges based on the bulkSize
function range_array(bulk_size, total_rows, callback) {
    var arr2 = []
    arr2.push(0)
    c = Math.floor(total_rows / bulk_size)
    x = bulk_size
    for (i = 0; i < c; i++) {
        arr2.push(bulk_size)
        bulk_size = bulk_size + x
    }
    arr2.push(total_rows)
    callback(undefined, arr2)
}


bulkSize = 1000
// divide range array 
q = `select count(firstname) as total_rows from PAN_INDIA_IGNORE`
Mainfunction(q, (err, total_rows) => {

    // return divide range array 
    range_array(bulkSize, total_rows, (err, result_array) => {

        //extracting data from mysql 
        // TODO : rename the function
        extract_row_data(result_array, (error, result) => {
            if (error) return console.log("error from extract_row_data" + error.message)
        })
    })
})


/*
  Extracting the from the MYSQL for each range(stored in the item argument) 
  and then calling search_es for that range 
    */
function extract_row_data(item, callback) {

    // item is an array of range divide on bulkSize
    for (i = 0; i < item.length - 1; i++) {

        // Decide the limit of data for two consecutive element of item array
        limit = item[i + 1] - item[i]
        q = `select firstName from PAN_INDIA_IGNORE where id > ${item[i]} and id <= ${item[i+1]} limit ${limit}`

        con.query(q, (err, result) => {
            if (err) return console.log('FAILED')

            // creating an empty array to store the first name from raw data received from the Mysql
            firstname_array = []
            for (j = 0; j < result.length; j++) {
                firstname_array.push(result[j].firstName)
            }

            // Passing the array of the first name(firstname_array) in async loop over serch_es function    
            asyncLoop(firstname_array, search_es)
        })
    }
}





















// utility function to remove the last character i.e ","
function remove_last(str) {
    str2 = ""

    for (i = 0; i < str.length - 1; i++) {
        str2 = str2 + str[i]
    }

    return str2
}